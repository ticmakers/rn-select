[@ticmakers-react-native/select](../README.md) > ["styles"](../modules/_styles_.md)

# External module: "styles"

## Index

### Variables

* [styles](_styles_.md#styles)
* [stylesSelect](_styles_.md#stylesselect)

### Object literals

* [colors](_styles_.md#colors)

---

## Variables

<a id="styles"></a>

### `<Const>` styles

**● styles**: *`object`* =  StyleSheet.create({
  container: {
  },

  item: {
    alignItems: 'center',
    alignSelf: 'center',
    width: '100%',
  },

  itemContainer: {

  },

  iconError: {
    color: colors.error,
  },

  iconSuccess: {
    color: colors.success,
  },

  message: {
    paddingLeft: 8,
    paddingRight: 8,
  },

  messageError: {
    color: colors.error,
  },

  messageSuccess: {
    color: colors.success,
  },
})

*Defined in styles.ts:8*

#### Type declaration

 container: `object`

 itemContainer: `object`

 iconError: `object`

 color: `string`

 iconSuccess: `object`

 color: `string`

 item: `object`

 alignItems: "center"

 alignSelf: "center"

 width: `string`

 message: `object`

 paddingLeft: `number`

 paddingRight: `number`

 messageError: `object`

 color: `string`

 messageSuccess: `object`

 color: `string`

___
<a id="stylesselect"></a>

### `<Const>` stylesSelect

**● stylesSelect**: *`object`* =  StyleSheet.create({
  iconContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    right: 8,
  },

  inputAndroidContainer: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },

  inputAndroid: {
    // borderColor: 'silver',
    // borderRadius: 8,
    // borderWidth: .5,
    color: 'black',
    fontSize: 16,
    // paddingHorizontal: 4,
    paddingLeft: 8,
    // marginLeft: 8,
    paddingRight: 30, // to ensure the text is never behind the icon
    paddingVertical: 11,
    width: '100%',
  },

  inputIOS: {
    // borderColor: 'gray',
    // borderRadius: 4,
    // borderWidth: 1,
    color: 'black',
    fontSize: 16,
    paddingHorizontal: 10,
    paddingRight: 30, // to ensure the text is never behind the icon
    paddingVertical: 12,
  },
})

*Defined in styles.ts:44*

#### Type declaration

 iconContainer: `object`

 alignItems: "center"

 justifyContent: "center"

 position: "absolute"

 right: `number`

 inputAndroid: `object`

 color: `string`

 fontSize: `number`

 paddingLeft: `number`

 paddingRight: `number`

 paddingVertical: `number`

 width: `string`

 inputAndroidContainer: `object`

 alignItems: "center"

 flex: `number`

 flexDirection: "row"

 justifyContent: "center"

 inputIOS: `object`

 color: `string`

 fontSize: `number`

 paddingHorizontal: `number`

 paddingRight: `number`

 paddingVertical: `number`

___

## Object literals

<a id="colors"></a>

### `<Const>` colors

**colors**: *`object`*

*Defined in styles.ts:3*

<a id="colors.error"></a>

####  error

**● error**: *`string`* = "#e8333f"

*Defined in styles.ts:4*

___
<a id="colors.success"></a>

####  success

**● success**: *`string`* = "#3f8445"

*Defined in styles.ts:5*

___

___

