[@ticmakers-react-native/select](../README.md) > ["Select"](../modules/_select_.md) > [Select](../classes/_select_.select.md)

# Class: Select

## Type parameters
#### SS 
## Hierarchy

 `ValidationComponent`<`ISelectProps`, `ISelectState`>

**↳ Select**

## Index

### Constructors

* [constructor](_select_.select.md#constructor)

### Properties

* [errors](_select_.select.md#errors)
* [fieldName](_select_.select.md#fieldname)
* [locale](_select_.select.md#locale)
* [messages](_select_.select.md#messages)
* [rules](_select_.select.md#rules)
* [value](_select_.select.md#value)

### Methods

* [IconError](_select_.select.md#iconerror)
* [IconLeft](_select_.select.md#iconleft)
* [IconRight](_select_.select.md#iconright)
* [IconSuccess](_select_.select.md#iconsuccess)
* [Label](_select_.select.md#label)
* [MessageInput](_select_.select.md#messageinput)
* [Select](_select_.select.md#select)
* [SelectNative](_select_.select.md#selectnative)
* [UNSAFE_componentWillMount](_select_.select.md#unsafe_componentwillmount)
* [UNSAFE_componentWillReceiveProps](_select_.select.md#unsafe_componentwillreceiveprops)
* [UNSAFE_componentWillUpdate](_select_.select.md#unsafe_componentwillupdate)
* [_onChange](_select_.select.md#_onchange)
* [_processProps](_select_.select.md#_processprops)
* [componentDidCatch](_select_.select.md#componentdidcatch)
* [componentDidMount](_select_.select.md#componentdidmount)
* [componentDidUpdate](_select_.select.md#componentdidupdate)
* [componentWillMount](_select_.select.md#componentwillmount)
* [componentWillReceiveProps](_select_.select.md#componentwillreceiveprops)
* [componentWillUnmount](_select_.select.md#componentwillunmount)
* [componentWillUpdate](_select_.select.md#componentwillupdate)
* [getError](_select_.select.md#geterror)
* [getSnapshotBeforeUpdate](_select_.select.md#getsnapshotbeforeupdate)
* [hasError](_select_.select.md#haserror)
* [hasErrors](_select_.select.md#haserrors)
* [isValid](_select_.select.md#isvalid)
* [render](_select_.select.md#render)
* [shouldComponentUpdate](_select_.select.md#shouldcomponentupdate)
* [validate](_select_.select.md#validate)

---

## Constructors

<a id="constructor"></a>

###  constructor

⊕ **new Select**(props: *`ISelectProps`*): [Select](_select_.select.md)

*Defined in Select.tsx:16*

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| props | `ISelectProps` |  An object of the SelectProps |

**Returns:** [Select](_select_.select.md)

___

## Properties

<a id="errors"></a>

###  errors

**● errors**: *`IValidationError`[]*

*Inherited from ValidationComponent.errors*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Select/node_modules/@ticmakers-react-native/core/src/Validation/index.d.ts:137*

___
<a id="fieldname"></a>

### `<Optional>` fieldName

**● fieldName**: *`undefined` \| `string`*

*Inherited from ValidationComponent.fieldName*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Select/node_modules/@ticmakers-react-native/core/src/Validation/index.d.ts:144*

___
<a id="locale"></a>

### `<Optional>` locale

**● locale**: *`undefined` \| `string`*

*Inherited from ValidationComponent.locale*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Select/node_modules/@ticmakers-react-native/core/src/Validation/index.d.ts:130*

___
<a id="messages"></a>

###  messages

**● messages**: *`IValidationLocale`*

*Inherited from ValidationComponent.messages*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Select/node_modules/@ticmakers-react-native/core/src/Validation/index.d.ts:151*

___
<a id="rules"></a>

###  rules

**● rules**: *`IValidationRules`*

*Inherited from ValidationComponent.rules*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Select/node_modules/@ticmakers-react-native/core/src/Validation/index.d.ts:158*

___
<a id="value"></a>

###  value

**● value**: *`TypeValidationValue`*

*Inherited from ValidationComponent.value*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Select/node_modules/@ticmakers-react-native/core/src/Validation/index.d.ts:165*

___

## Methods

<a id="iconerror"></a>

###  IconError

▸ **IconError**(): `TypeComponent`

*Defined in Select.tsx:217*

**Returns:** `TypeComponent`

___
<a id="iconleft"></a>

###  IconLeft

▸ **IconLeft**(): `TypeComponent`

*Defined in Select.tsx:176*

**Returns:** `TypeComponent`

___
<a id="iconright"></a>

###  IconRight

▸ **IconRight**(): `TypeComponent`

*Defined in Select.tsx:191*

**Returns:** `TypeComponent`

___
<a id="iconsuccess"></a>

###  IconSuccess

▸ **IconSuccess**(): `TypeComponent`

*Defined in Select.tsx:206*

**Returns:** `TypeComponent`

___
<a id="label"></a>

###  Label

▸ **Label**(): `TypeComponent`

*Defined in Select.tsx:81*

**Returns:** `TypeComponent`

___
<a id="messageinput"></a>

###  MessageInput

▸ **MessageInput**(): `TypeComponent`

*Defined in Select.tsx:228*

**Returns:** `TypeComponent`

___
<a id="select"></a>

###  Select

▸ **Select**(): `TypeComponent`

*Defined in Select.tsx:94*

**Returns:** `TypeComponent`

___
<a id="selectnative"></a>

###  SelectNative

▸ **SelectNative**(): `TypeComponent`

*Defined in Select.tsx:130*

**Returns:** `TypeComponent`

___
<a id="unsafe_componentwillmount"></a>

### `<Optional>` UNSAFE_componentWillMount

▸ **UNSAFE_componentWillMount**(): `void`

*Inherited from DeprecatedLifecycle.UNSAFE_componentWillMount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Select/node_modules/@types/react/index.d.ts:660*

**Returns:** `void`

___
<a id="unsafe_componentwillreceiveprops"></a>

### `<Optional>` UNSAFE_componentWillReceiveProps

▸ **UNSAFE_componentWillReceiveProps**(nextProps: *`Readonly`<`IValidationProps` & `ISelectProps`>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.UNSAFE_componentWillReceiveProps*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Select/node_modules/@types/react/index.d.ts:692*

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<`IValidationProps` & `ISelectProps`> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="unsafe_componentwillupdate"></a>

### `<Optional>` UNSAFE_componentWillUpdate

▸ **UNSAFE_componentWillUpdate**(nextProps: *`Readonly`<`IValidationProps` & `ISelectProps`>*, nextState: *`Readonly`<`ISelectState`>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.UNSAFE_componentWillUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Select/node_modules/@types/react/index.d.ts:720*

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<`IValidationProps` & `ISelectProps`> |
| nextState | `Readonly`<`ISelectState`> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="_onchange"></a>

### `<Private>` _onChange

▸ **_onChange**(value: *`any`*, index: *`number`*): `void`

*Defined in Select.tsx:271*

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| value | `any` |  Current value selected |
| index | `number` |  Current index selected |

**Returns:** `void`

___
<a id="_processprops"></a>

### `<Private>` _processProps

▸ **_processProps**(): `ISelectProps`

*Defined in Select.tsx:289*

**Returns:** `ISelectProps`

___
<a id="componentdidcatch"></a>

### `<Optional>` componentDidCatch

▸ **componentDidCatch**(error: *`Error`*, errorInfo: *`ErrorInfo`*): `void`

*Inherited from ComponentLifecycle.componentDidCatch*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Select/node_modules/@types/react/index.d.ts:589*

**Parameters:**

| Name | Type |
| ------ | ------ |
| error | `Error` |
| errorInfo | `ErrorInfo` |

**Returns:** `void`

___
<a id="componentdidmount"></a>

### `<Optional>` componentDidMount

▸ **componentDidMount**(): `void`

*Inherited from ComponentLifecycle.componentDidMount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Select/node_modules/@types/react/index.d.ts:568*

**Returns:** `void`

___
<a id="componentdidupdate"></a>

### `<Optional>` componentDidUpdate

▸ **componentDidUpdate**(prevProps: *`Readonly`<`IValidationProps` & `ISelectProps`>*, prevState: *`Readonly`<`ISelectState`>*, snapshot: *[SS]()*): `void`

*Inherited from NewLifecycle.componentDidUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Select/node_modules/@types/react/index.d.ts:631*

**Parameters:**

| Name | Type |
| ------ | ------ |
| prevProps | `Readonly`<`IValidationProps` & `ISelectProps`> |
| prevState | `Readonly`<`ISelectState`> |
| `Optional` snapshot | [SS]() |

**Returns:** `void`

___
<a id="componentwillmount"></a>

### `<Optional>` componentWillMount

▸ **componentWillMount**(): `void`

*Inherited from DeprecatedLifecycle.componentWillMount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Select/node_modules/@types/react/index.d.ts:646*

**Returns:** `void`

___
<a id="componentwillreceiveprops"></a>

### `<Optional>` componentWillReceiveProps

▸ **componentWillReceiveProps**(nextProps: *`Readonly`<`IValidationProps` & `ISelectProps`>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.componentWillReceiveProps*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Select/node_modules/@types/react/index.d.ts:675*

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<`IValidationProps` & `ISelectProps`> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="componentwillunmount"></a>

### `<Optional>` componentWillUnmount

▸ **componentWillUnmount**(): `void`

*Inherited from ComponentLifecycle.componentWillUnmount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Select/node_modules/@types/react/index.d.ts:584*

**Returns:** `void`

___
<a id="componentwillupdate"></a>

### `<Optional>` componentWillUpdate

▸ **componentWillUpdate**(nextProps: *`Readonly`<`IValidationProps` & `ISelectProps`>*, nextState: *`Readonly`<`ISelectState`>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.componentWillUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Select/node_modules/@types/react/index.d.ts:705*

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<`IValidationProps` & `ISelectProps`> |
| nextState | `Readonly`<`ISelectState`> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="geterror"></a>

###  getError

▸ **getError**(ruleName: *`string`*): `string` \| `null`

*Inherited from ValidationComponent.getError*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Select/node_modules/@ticmakers-react-native/core/src/Validation/index.d.ts:203*

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| ruleName | `string` |  name of the rule |

**Returns:** `string` \| `null`

___
<a id="getsnapshotbeforeupdate"></a>

### `<Optional>` getSnapshotBeforeUpdate

▸ **getSnapshotBeforeUpdate**(prevProps: *`Readonly`<`IValidationProps` & `ISelectProps`>*, prevState: *`Readonly`<`ISelectState`>*): `SS` \| `null`

*Inherited from NewLifecycle.getSnapshotBeforeUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Select/node_modules/@types/react/index.d.ts:625*

**Parameters:**

| Name | Type |
| ------ | ------ |
| prevProps | `Readonly`<`IValidationProps` & `ISelectProps`> |
| prevState | `Readonly`<`ISelectState`> |

**Returns:** `SS` \| `null`

___
<a id="haserror"></a>

###  hasError

▸ **hasError**(ruleName: *`string`*): `boolean`

*Inherited from ValidationComponent.hasError*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Select/node_modules/@ticmakers-react-native/core/src/Validation/index.d.ts:188*

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| ruleName | `string` |  name of the rule |

**Returns:** `boolean`

___
<a id="haserrors"></a>

###  hasErrors

▸ **hasErrors**(): `boolean`

*Inherited from ValidationComponent.hasErrors*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Select/node_modules/@ticmakers-react-native/core/src/Validation/index.d.ts:180*

**Returns:** `boolean`

___
<a id="isvalid"></a>

###  isValid

▸ **isValid**(): `boolean`

*Inherited from ValidationComponent.isValid*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Select/node_modules/@ticmakers-react-native/core/src/Validation/index.d.ts:195*

**Returns:** `boolean`

___
<a id="render"></a>

###  render

▸ **render**(): `TypeComponent`

*Defined in Select.tsx:38*

**Returns:** `TypeComponent`

___
<a id="shouldcomponentupdate"></a>

### `<Optional>` shouldComponentUpdate

▸ **shouldComponentUpdate**(nextProps: *`Readonly`<`IValidationProps` & `ISelectProps`>*, nextState: *`Readonly`<`ISelectState`>*, nextContext: *`any`*): `boolean`

*Inherited from ComponentLifecycle.shouldComponentUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Select/node_modules/@types/react/index.d.ts:579*

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<`IValidationProps` & `ISelectProps`> |
| nextState | `Readonly`<`ISelectState`> |
| nextContext | `any` |

**Returns:** `boolean`

___
<a id="validate"></a>

###  validate

▸ **validate**(rules: *`IValidationInputRules`*): `boolean`

*Inherited from ValidationComponent.validate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Select/node_modules/@ticmakers-react-native/core/src/Validation/index.d.ts:173*

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| rules | `IValidationInputRules` |  Object to define the rules of the input |

**Returns:** `boolean`

___

