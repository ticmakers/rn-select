"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var react_native_1 = require("react-native");
var colors = {
    error: '#e8333f',
    success: '#3f8445',
};
exports.styles = react_native_1.StyleSheet.create({
    container: {},
    item: {
        alignItems: 'center',
        alignSelf: 'center',
        width: '100%',
    },
    itemContainer: {},
    iconError: {
        color: colors.error,
    },
    iconSuccess: {
        color: colors.success,
    },
    message: {
        paddingLeft: 8,
        paddingRight: 8,
    },
    messageError: {
        color: colors.error,
    },
    messageSuccess: {
        color: colors.success,
    },
});
exports.stylesSelect = react_native_1.StyleSheet.create({
    iconContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        right: 8,
    },
    inputAndroidContainer: {
        alignItems: 'center',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    inputAndroid: {
        color: 'black',
        fontSize: 16,
        paddingLeft: 8,
        paddingRight: 30,
        paddingVertical: 11,
        width: '100%',
    },
    inputIOS: {
        color: 'black',
        fontSize: 16,
        paddingHorizontal: 10,
        paddingRight: 30,
        paddingVertical: 12,
    },
});
exports.default = exports.styles;
//# sourceMappingURL=styles.js.map