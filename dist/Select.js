"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_native_1 = require("react-native");
var native_base_1 = require("native-base");
var react_native_picker_select_1 = require("react-native-picker-select");
var core_1 = require("@ticmakers-react-native/core");
var icon_1 = require("@ticmakers-react-native/icon");
var styles_1 = require("./styles");
var Select = (function (_super) {
    __extends(Select, _super);
    function Select(props) {
        var _this = this;
        var error = props.error, success = props.success, value = props.value;
        _this = _super.call(this, props) || this;
        _this.state = {
            error: typeof error !== 'undefined' ? error : false,
            success: typeof success !== 'undefined' ? success : false,
            value: typeof value !== 'undefined' ? value : undefined,
        };
        _this.locale = _this.props.lang || 'en';
        return _this;
    }
    Select.prototype.render = function () {
        var _a = this._processProps(), containerStyle = _a.containerStyle, fixed = _a.fixed, floating = _a.floating, hideValidationIcons = _a.hideValidationIcons, inline = _a.inline, last = _a.last, regular = _a.regular, rounded = _a.rounded, stacked = _a.stacked;
        var _b = this.state, error = _b.error, success = _b.success;
        var containerProps = {
            style: react_native_1.StyleSheet.flatten([styles_1.styles.container, containerStyle]),
        };
        var itemProps = {
            error: error,
            last: last,
            regular: regular,
            rounded: rounded,
            success: success,
            fixedLabel: fixed,
            floatingLabel: floating,
            inlineLabel: inline,
            picker: true,
            stackedLabel: stacked,
            style: react_native_1.StyleSheet.flatten([styles_1.styles.item]),
        };
        return (React.createElement(react_native_1.View, __assign({}, containerProps),
            React.createElement(native_base_1.Item, __assign({}, itemProps),
                this.IconLeft(),
                this.Label(),
                this.SelectNative(),
                this.IconRight(),
                (success && !hideValidationIcons) && this.IconSuccess(),
                (error && !hideValidationIcons) && this.IconError()),
            this.MessageInput()));
    };
    Select.prototype.Label = function () {
        var _a = this._processProps(), fixed = _a.fixed, inline = _a.inline, floating = _a.floating, label = _a.label, stacked = _a.stacked;
        var canShow = (fixed || inline || floating || stacked);
        if (canShow) {
            return React.createElement(native_base_1.Label, null, label);
        }
    };
    Select.prototype.Select = function () {
        var _a = this._processProps(), disabled = _a.disabled, iconArrow = _a.iconArrow, items = _a.items, pickerProps = _a.pickerProps, placeholder = _a.placeholder, selected = _a.selected, style = _a.style, textInputProps = _a.textInputProps, useNativeAndroidPickerStyle = _a.useNativeAndroidPickerStyle, value = _a.value;
        var _iconArrow = iconArrow;
        var iconArrowName = react_native_1.Platform.select({ android: 'menu-down', ios: 'ios-arrow-down' });
        var iconArrowType = react_native_1.Platform.select({ android: 'material-community', ios: 'ionicon' });
        var _placeholder = { label: placeholder || 'Select a item', value: null, color: '#9EA0A4' };
        if (!core_1.AppHelper.isComponent(iconArrow) && iconArrow && Object.keys(iconArrow).length > 0) {
            _iconArrow = React.createElement(icon_1.default, __assign({}, iconArrow));
        }
        else if (!iconArrow) {
            _iconArrow = React.createElement(icon_1.default, { name: iconArrowName, type: iconArrowType });
        }
        var props = {
            disabled: disabled,
            items: items,
            pickerProps: pickerProps,
            textInputProps: textInputProps,
            useNativeAndroidPickerStyle: useNativeAndroidPickerStyle,
            value: value,
            Icon: _iconArrow && (function () { return _iconArrow; }),
            itemKey: selected,
            onValueChange: this._onChange.bind(this),
            placeholder: _placeholder,
            style: __assign(__assign({}, styles_1.stylesSelect), style),
        };
        return React.createElement(react_native_picker_select_1.default, __assign({}, props));
    };
    Select.prototype.SelectNative = function () {
        var _a = this._processProps(), disabled = _a.disabled, iconArrow = _a.iconArrow, items = _a.items, pickerProps = _a.pickerProps, placeholder = _a.placeholder, selected = _a.selected, style = _a.style, textInputProps = _a.textInputProps, useNativeAndroidPickerStyle = _a.useNativeAndroidPickerStyle;
        var value = this.state.value;
        var _iconArrow = iconArrow;
        var iconArrowName = react_native_1.Platform.select({ android: 'menu-down', ios: 'ios-arrow-down' });
        var iconArrowType = react_native_1.Platform.select({ android: 'material-community', ios: 'ionicon' });
        var _placeholder = { label: placeholder || 'Select a item', value: null, color: '#9EA0A4' };
        if (!core_1.AppHelper.isComponent(iconArrow) && iconArrow && Object.keys(iconArrow).length > 0) {
            _iconArrow = React.createElement(icon_1.default, __assign({}, iconArrow));
        }
        else if (!iconArrow) {
            _iconArrow = React.createElement(icon_1.default, { name: iconArrowName, type: iconArrowType });
        }
        var props = {
            placeholder: placeholder,
            onValueChange: this._onChange.bind(this),
            selectedValue: value || selected,
            style: __assign({ width: '100%' }, style),
        };
        return (React.createElement(native_base_1.Picker, __assign({}, props),
            React.createElement(native_base_1.Picker.Item, { label: placeholder || 'Select an item', value: null, color: "#9EA0A4" }),
            !!items && items.map(function (i) { return (React.createElement(native_base_1.Picker.Item, { label: i.label, value: i.value, key: i.key })); })));
    };
    Select.prototype.IconLeft = function () {
        var iconLeft = this._processProps().iconLeft;
        if (core_1.AppHelper.isComponent(iconLeft)) {
            return React.cloneElement(iconLeft, {});
        }
        else if (iconLeft && Object.keys(iconLeft).length > 0) {
            return React.createElement(icon_1.default, __assign({}, iconLeft));
        }
    };
    Select.prototype.IconRight = function () {
        var iconRight = this._processProps().iconRight;
        if (core_1.AppHelper.isComponent(iconRight)) {
            return React.cloneElement(iconRight, {});
        }
        else if (iconRight && Object.keys(iconRight).length > 0) {
            return React.createElement(icon_1.default, __assign({}, iconRight));
        }
    };
    Select.prototype.IconSuccess = function () {
        var iconName = react_native_1.Platform.select({ android: 'check-circle', ios: 'ios-checkmark-circle' });
        var iconType = react_native_1.Platform.select({ android: 'material-community', ios: 'ionicon' });
        return React.createElement(icon_1.default, { name: iconName, type: iconType, color: styles_1.styles.iconSuccess.color });
    };
    Select.prototype.IconError = function () {
        var iconName = react_native_1.Platform.select({ android: 'close-circle', ios: 'ios-close-circle' });
        var iconType = react_native_1.Platform.select({ android: 'material-community', ios: 'ionicon' });
        return React.createElement(icon_1.default, { name: iconName, type: iconType, color: styles_1.styles.iconError.color });
    };
    Select.prototype.MessageInput = function () {
        var _a = this._processProps(), msgError = _a.msgError, msgSuccess = _a.msgSuccess;
        var _b = this.state, error = _b.error, success = _b.success;
        if (success && msgSuccess) {
            return React.createElement(native_base_1.Text, { style: react_native_1.StyleSheet.flatten([styles_1.styles.message, styles_1.styles.messageSuccess]) }, msgSuccess);
        }
        if (error) {
            var isDate = this.hasError('date');
            var isEmail = this.hasError('email');
            var isMaxLength = this.hasError('maxLength');
            var isMinLength = this.hasError('minLength');
            var isNumbers = this.hasError('numbers');
            var isRequired = this.hasError('required');
            var message = '';
            if (isRequired) {
                message = this.getError('required') || '';
            }
            else if (!isRequired && isMinLength) {
                message = this.getError('minLength') || '';
            }
            else if (!isRequired && isMaxLength) {
                message = this.getError('maxLength') || '';
            }
            else if ((!isRequired && !isMinLength) && (isEmail || isDate || isNumbers)) {
                if (isDate) {
                    message = this.getError('date') || '';
                }
                else if (isEmail) {
                    message = this.getError('email') || '';
                }
                else if (isNumbers) {
                    message = this.getError('numbers') || '';
                }
            }
            return React.createElement(native_base_1.Text, { style: react_native_1.StyleSheet.flatten([styles_1.styles.message, styles_1.styles.messageError]) }, msgError || message);
        }
    };
    Select.prototype._onChange = function (value, index) {
        var rules = this._processProps().rules;
        var onChange = this.props.onChange;
        this.value = value;
        this.validate(rules || {});
        this.setState({ value: value, error: this.hasErrors(), success: !this.hasErrors() });
        if (onChange) {
            return onChange(value, index);
        }
    };
    Select.prototype._processProps = function () {
        var _a = this.props, disabled = _a.disabled, doneText = _a.doneText, fixed = _a.fixed, floating = _a.floating, hideDoneBar = _a.hideDoneBar, hideValidationIcons = _a.hideValidationIcons, inline = _a.inline, items = _a.items, label = _a.label, lang = _a.lang, last = _a.last, regular = _a.regular, rounded = _a.rounded, stacked = _a.stacked, useNativeAndroidPickerStyle = _a.useNativeAndroidPickerStyle, value = _a.value;
        var props = __assign(__assign({}, this.props), { disabled: typeof disabled !== 'undefined' ? disabled : false, doneText: typeof doneText !== 'undefined' ? doneText : 'Done', fixed: typeof fixed !== 'undefined' ? fixed : false, floating: typeof floating !== 'undefined' ? floating : false, hideDoneBar: typeof hideDoneBar !== 'undefined' ? hideDoneBar : false, hideValidationIcons: typeof hideValidationIcons !== 'undefined' ? hideValidationIcons : true, inline: typeof inline !== 'undefined' ? inline : false, items: typeof items !== 'undefined' ? items : [], lang: typeof lang !== 'undefined' ? lang : 'en', last: typeof last !== 'undefined' ? last : false, regular: typeof regular !== 'undefined' ? regular : false, rounded: typeof rounded !== 'undefined' ? rounded : false, stacked: typeof stacked !== 'undefined' ? stacked : false, useNativeAndroidPickerStyle: typeof useNativeAndroidPickerStyle !== 'undefined' ? useNativeAndroidPickerStyle : false });
        this.fieldName = label;
        this.value = value;
        return props;
    };
    return Select;
}(core_1.ValidationComponent));
exports.default = Select;
//# sourceMappingURL=Select.js.map