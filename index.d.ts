import * as React from 'react'
import { ViewProps, PickerProps, TextInputProps } from 'react-native'

import { TypeComponent, TypeStyle, ValidationComponent } from '@ticmakers-react-native/core'
import { IIconProps } from '@ticmakers-react-native/icon'

/**
 * Interface to define the type of item for the select
 * @interface IItemType
 */
export interface IItemType {
  /**
   * A string to define the label of the item
   * @type {string}
   * @memberof IItemType
   */
  label: string

  /**
   * Define the value of the item
   * @type {*}
   * @memberof IItemType
   */
  value: any

  /**
   * Define a key to the item
   * @type {*}
   * @memberof IItemType
   */
  key?: any

  /**
   * Define a color to render the item
   * @type {string}
   * @memberof IItemType
   */
  color?: string
}

/**
 * Interface to define the input rules for the select
 * @interface IInputRules
 */
export interface IInputRules {
  /**
   * Set true to validate as date
   * @type {(boolean | string | RegExp)}
   * @memberof IInputRules
   */
  date?: boolean | string | RegExp

  /**
   * Set true to validate as email
   * @type {(boolean | RegExp)}
   * @memberof IInputRules
   */
  email?: boolean | RegExp

  /**
   * Set a number to validate as max length
   * @type {number}
   * @memberof IInputRules
   */
  maxLength?: number

  /**
   * Set a number to validate as min length
   * @type {number}
   * @memberof IInputRules
   */
  minLength?: number

  /**
   * Set true to validate as number
   * @type {(boolean | RegExp)}
   * @memberof IInputRules
   */
  numbers?: boolean | RegExp

  /**
   * Set a RegExp to validate as pattern
   * @type {RegExp}
   * @memberof IInputRules
   */
  pattern?: RegExp

  /**
   * Set true to validate as required
   * @type {true}
   * @memberof IInputRules
   */
  required?: true
}

/**
 * Interface to define the states of the Select component
 * @export
 * @interface ISelectState
 */
export interface ISelectState {
  /**
   * Set true to define if the validation is error
   * @type {boolean}
   * @default false
   */
  error?: boolean

  /**
   * Set true to define the validations as success
   * @type {boolean}
   */
  success?: boolean

  /**
   * Will attempt to locate a matching item from the `items` array by checking each item's `value` property.
   * If found, it will update the component to show that item as selected.
   * If the `value` is not found, it will default to the first item.
   * @type {*}
   */
  value?: any
}

/**
 * Interface to define the props of the Select component
 * @export
 * @interface ISelectProps
 * @extends {ISelectState}
 */
export interface ISelectProps extends ISelectState {
  /**
   * Apply a custom style to the container of the select
   * @type {TypeStyle}
   */
  containerStyle?: TypeStyle

  /**
   * If set to true, the picker will be disabled, i.e. the user will not be able to make a selection.
   * @type {boolean}
   * @default false
   */
  disabled?: boolean

  /**
   * "Done" default text on the modal. Can be overwritten here
   * @type {string}
   * @default Done
   * @platform iOS
   */
  doneText?: string

  /**
   * Set true to define if the validation is error
   * @type {boolean}
   * @default false
   */
  error?: boolean

  /**
   * Set true to define the label as fixed style
   * @type {boolean}
   * @default false
   */
  fixed?: boolean

  /**
   * Set true to define the label as floating style
   * @type {boolean}
   * @default false
   */
  floating?: boolean

  /**
   * Hides the bar with tabbing arrows and Done link to exit the modal.
   * While this is typical on `select` elements on the web, the interface guidelines does not include it.
   * @type {boolean}
   * @platform iOS
   */
  hideDoneBar?: boolean

  /**
   * Set true to hide the icons validations
   * @type {boolean}
   * @default false
   */
  hideValidationIcons?: boolean

  /**
   * A React-Native or an object of the IconProps to define the icon arrow
   * @type {(TypeComponent | IIconProps)}
   */
  iconArrow?: TypeComponent | IIconProps

  /**
   * A React-Native component or an object with the props of the icon to render the icon to the left
   * @type {(TypeComponent | IIconProps)}
   */
  iconLeft?: TypeComponent | IIconProps

  /**
   * A React-Native component or an object with the props of the icon to render the icon to the right
   * @type {(TypeComponent | IIconProps)}
   */
  iconRight?: TypeComponent | IIconProps

  /**
   * Set true to define the label as inline style
   * @type {boolean}
   * @default false
   */
  inline?: boolean

  /**
   * The items for the component to render
   * - Each item should be in the following format:
   * `{label: 'Orange', value: 'orange', key: 'orange', color: 'orange'}`
   * - The label and the value are required
   * - The key and color are optional
   * - The key will be set to the label if not included
   * - The value can be any data type
   * - Must have at least one item
   * @type {IItemType[]}
   */
  items: IItemType[]

  /**
   * Style to apply to each of the item labels.
   * @type {TypeStyle}
   * @platform iOS
   */
  itemStyle?: TypeStyle

  /**
   * The field name to show as label
   * @type {string}
   */
  label?: string

  /**
   * Define a language to show the messages
   * @type {('en' | 'es' | 'fr')}
   * @default en
   */
  lang?: 'en' | 'es' | 'fr'

  /**
   * Set true to define the select as the last element
   * @type {boolean}
   * @default false
   */
  last?: boolean

  /**
   * Additional props to pass to the Modal (some props are used in core functionality so use this carefully)
   * @type {object}
   */
  modalProps?: object

  /**
   * On Android, specifies how to display the selection items when the user taps on the picker:
   * - `dialog`: Show a modal dialog. This is the default.
   * - `dropdown`: Shows a dropdown anchored to the picker view
   * @type {string}
   * @platform Android
   */
  mode?: string

  /**
   * Set a custom message to show as error validation
   * @type {string}
   */
  msgError?: string

  /**
   * Set a custom message to show as success validation
   * @type {string}
   */
  msgSuccess?: string

  /**
   * Callback for when an item is selected. This is called with the following parameters:
   * - `itemValue`: the `value` prop of the item that was selected
   * - `itemPosition`: the index of the selected item in this picker
   */
  onChange?: (value: any, position?: number) => any

  /**
   * Callback triggered right before the opening or closing of the picker
   */
  onClose?: () => any

  /**
   * Callback when the 'Done' button is pressed
   * @platform iOS
   */
  onDonePress?: () => any

  /**
   * Presence enables the corresponding arrow
   * - Closes the picker
   * - Calls the callback provided
   */
  onDownArrow?: () => any

  /**
   * Callback triggered right before the opening or closing of the picker
   */
  onOpen?: () => any

  /**
   * Action that fire when the icon left is pressed
   */
  onPressIconLeft?: () => any

  /**
   * Action that fire when the icon right is pressed
   */
  onPressIconRight?: () => any

  /**
   * Presence enables the corresponding arrow
   * - Closes the picker
   * - Calls the callback provided
   * @platform iOS
   */
  onUpArrow?: () => any

  /**
   * Additional props to pass to the Picker (some props are used in core functionality so use this carefully)
   * @type {PickerProps}
   */
  pickerProps?: PickerProps

  /**
   * - An override for the default placeholder object with a label of `Select an item...` and a value of `null`
   * - An empty object can be used if you'd like to disable the placeholder entirely
   * @type {string}
   */
  placeholder?: string

  /**
   * Set true to define the style of the input as regular
   * @type {boolean}
   * @default false
   */
  regular?: boolean

  /**
   * Set true to define the style  of the input as rounded
   * @type {boolean}
   * @default false
   */
  rounded?: boolean

  /**
   * Object to define  the rules of the select
   * @type {IInputRules}
   */
  rules?: IInputRules

  /**
   * Will attempt to locate a matching item from the `items` array by checking each item's `key` property.
   * If found, it will update the component to show that item as selected.
   * If the `key` is not found, it will attempt to find a matching item by value as above.
   * @type {*}
   */
  selected?: any

  /**
   * Set true to define the label as stacked style
   * @type {boolean}
   */
  stacked?: boolean

  /**
   * Style overrides for most parts of the component.
   * @type {TypeStyle}
   */
  style?: TypeStyle

  /**
   * Set true to define the validations as success
   * @type {boolean}
   */
  success?: boolean

  /**
   * Additional props to pass to the TextInput (some props are used in core functionality so use this carefully).
   * This is iOS only unless useNativeAndroidPickerStyle={false}.
   * @type {TextInputProps}
   */
  textInputProps?: TextInputProps

  /**
   * The component defaults to using the native Android Picker in its un-selected state.
   * Setting this flag to false will mimic the default iOS presentation where a tappable TextInput is displayed.
   * @type {boolean}
   * @platform Android
   *
   */
  useNativeAndroidPickerStyle?: boolean
}

/**
 * Class to define the Select component
 * @class Select
 * @extends {ValidationComponent<ISelectProps, ISelectState>}
 */
declare class Select extends ValidationComponent<ISelectProps, ISelectState> {
  /**
   * Method that renders the component
   * @returns {TypeComponent}
   * @memberof Select
   */
  public render(): TypeComponent

  /**
   * Method that renders the Label component
   * @returns {TypeComponent}
   * @memberof Select
   */
  public Label(): TypeComponent

  /**
   * Method that renders the Select component
   * @returns {TypeComponent}
   * @memberof Select
   */
  public Select(): TypeComponent

  /**
   * Method that renders the Icon left component
   * @returns {TypeComponent}
   * @memberof Select
   */
  public IconLeft(): TypeComponent

  /**
   * Method that renders the Icon right component
   * @returns {TypeComponent}
   * @memberof Select
   */
  public IconRight(): TypeComponent

  /**
   * Method that renders the Icon success component
   * @returns {TypeComponent}
   * @memberof Select
   */
  public IconSuccess(): TypeComponent

  /**
   * Method that renders the Icon error component
   * @returns {TypeComponent}
   * @memberof Select
   */
  public IconError(): TypeComponent

  /**
   * Method that renders the MessageInput component
   * @returns {TypeComponent}
   * @memberof Select
   */
  public MessageInput(): TypeComponent

  /**
   * Method that fire when the select changes
   * @private
   * @param {*} value    Current value selected
   * @param {number} index    Current index selected
   * @returns {void}
   * @memberof Select
   */
  private _onChange(value: any, index: number): void

  /**
   * Method to process the props
   * @private
   * @returns {ISelectState}
   * @memberof Select
   */
  private _processProps(): ISelectState
}

declare module '@ticmakers-react-native/select'

export default Select
