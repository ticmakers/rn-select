import * as React from 'react'
import { View, StyleSheet, ViewProps, Platform } from 'react-native'
import { Item, Label, Text, NativeBase, Picker } from 'native-base'
import RNPickerSelect, { PickerProps as RNPickerProps } from 'react-native-picker-select'

import { AppHelper, ValidationComponent, TypeComponent } from '@ticmakers-react-native/core'
import Icon, { IIconProps, TypeIcons } from '@ticmakers-react-native/icon'
import { ISelectProps, ISelectState } from './../index'
import { styles, stylesSelect } from './styles'

/**
 * Class to define the Select component
 * @class Select
 * @extends {ValidationComponent<ISelectProps, ISelectState>}
 */
export default class Select extends ValidationComponent<ISelectProps, ISelectState> {
  /**
   * Creates an instance of Select.
   * @param {ISelectProps} props    An object of the SelectProps
   */
  constructor(props: ISelectProps) {
    const { error, success, value } = props
    super(props)

    this.state = {
      error: typeof error !== 'undefined' ? error : false,
      success: typeof success !== 'undefined' ? success : false,
      value: typeof value !== 'undefined' ? value : undefined,
    }

    this.locale = this.props.lang || 'en'
  }

  /**
   * Method that renders the component
   * @returns {TypeComponent}
   */
  public render(): TypeComponent {
    const { containerStyle, fixed, floating, hideValidationIcons, inline, last, regular, rounded, stacked } = this._processProps()
    const { error, success } = this.state

    const containerProps: ViewProps = {
      style: StyleSheet.flatten([styles.container, containerStyle]),
    }

    const itemProps: NativeBase.Item = {
      error,
      last,
      regular,
      rounded,
      success,
      // tslint:disable-next-line: object-literal-sort-keys
      fixedLabel: fixed,
      floatingLabel: floating,
      inlineLabel: inline,
      picker: true,
      stackedLabel: stacked,
      style: StyleSheet.flatten([styles.item]),
    }

    return (
      <View { ...containerProps }>
        <Item { ...itemProps }>
          { this.IconLeft() }
          { this.Label() }
          { this.SelectNative() }
          { this.IconRight() }
          { (success && !hideValidationIcons) && this.IconSuccess() }
          { (error && !hideValidationIcons) && this.IconError() }
        </Item>

        { this.MessageInput() }
      </View>
    )
  }

  /**
   * Method that renders the Label component
   * @returns {TypeComponent}
   */
  public Label(): TypeComponent {
    const { fixed, inline, floating, label, stacked } = this._processProps()
    const canShow = (fixed || inline || floating || stacked)

    if (canShow) {
      return <Label>{ label }</Label>
    }
  }

  /**
   * Method that renders the Select component
   * @returns {TypeComponent}
   */
  public Select(): TypeComponent {
    const { disabled, iconArrow, items, pickerProps, placeholder, selected, style, textInputProps, useNativeAndroidPickerStyle, value } = this._processProps()

    let _iconArrow = iconArrow
    const iconArrowName = Platform.select({ android: 'menu-down', ios: 'ios-arrow-down' })
    const iconArrowType = Platform.select({ android: 'material-community', ios: 'ionicon' }) as TypeIcons
    const _placeholder = { label: placeholder || 'Select a item', value: null, color: '#9EA0A4' }

    if (!AppHelper.isComponent(iconArrow) && iconArrow && Object.keys(iconArrow).length > 0) {
      _iconArrow = <Icon { ...iconArrow as IIconProps } />
    } else if (!iconArrow) {
      _iconArrow = <Icon name={ iconArrowName as any } type={ iconArrowType }  />
    }

    const props: RNPickerProps = {
      disabled,
      items,
      pickerProps,
      textInputProps,
      useNativeAndroidPickerStyle,
      value,
      // tslint:disable-next-line: object-literal-sort-keys
      Icon: _iconArrow && (() => _iconArrow),
      itemKey: selected,
      onValueChange: this._onChange.bind(this),
      placeholder: _placeholder,
      style: { ...stylesSelect, ...style as object },
    }

    return <RNPickerSelect { ...props } />
  }

  /**
   * Method that renders the Select component
   * @returns {TypeComponent}
   */
  public SelectNative(): TypeComponent {
    const { disabled, iconArrow, items, pickerProps, placeholder, selected, style, textInputProps, useNativeAndroidPickerStyle } = this._processProps()
    const { value } = this.state

    let _iconArrow = iconArrow
    const iconArrowName = Platform.select({ android: 'menu-down', ios: 'ios-arrow-down' })
    const iconArrowType = Platform.select({ android: 'material-community', ios: 'ionicon' }) as TypeIcons
    const _placeholder = { label: placeholder || 'Select a item', value: null, color: '#9EA0A4' }

    if (!AppHelper.isComponent(iconArrow) && iconArrow && Object.keys(iconArrow).length > 0) {
      _iconArrow = <Icon { ...iconArrow as IIconProps } />
    } else if (!iconArrow) {
      _iconArrow = <Icon name={ iconArrowName as any } type={ iconArrowType }  />
    }

    const props: NativeBase.Picker = {
      // disabled,
      // items,
      // pickerProps,
      placeholder,
      // textInputProps,
      // useNativeAndroidPickerStyle,
      // value,
      // Icon: _iconArrow && (() => _iconArrow),
      // tslint:disable-next-line: object-literal-sort-keys
      onValueChange: this._onChange.bind(this),
      selectedValue: value || selected,
      // placeholder: _placeholder,
      style: { width: '100%', ...style as any },
    }

    return (
      <Picker { ...props }>
        <Picker.Item label={ placeholder || 'Select an item' } value={ null } color="#9EA0A4" />

        { !!items && items.map(i => (
          <Picker.Item label={ i.label } value={ i.value } key={ i.key } />
        )) }
      </Picker>
    )
  }

  /**
   * Method that renders the Icon left component
   * @returns {TypeComponent}
   */
  public IconLeft(): TypeComponent {
    const { iconLeft } = this._processProps()

    if (AppHelper.isComponent(iconLeft)) {
      return React.cloneElement(iconLeft as any, {})
    // tslint:disable-next-line: no-else-after-return
    } else if (iconLeft && Object.keys(iconLeft).length > 0) {
      return <Icon { ...iconLeft as IIconProps } />
    }
  }

  /**
   * Method that renders the Icon right component
   * @returns {TypeComponent}
   */
  public IconRight(): TypeComponent {
    const { iconRight } = this._processProps()

    if (AppHelper.isComponent(iconRight)) {
      return React.cloneElement(iconRight as any, {})
    // tslint:disable-next-line: no-else-after-return
    } else if (iconRight && Object.keys(iconRight).length > 0) {
      return <Icon { ...iconRight as IIconProps } />
    }
  }

  /**
   * Method that renders the Icon success component
   * @returns {TypeComponent}
   */
  public IconSuccess(): TypeComponent {
    const iconName = Platform.select({ android: 'check-circle', ios: 'ios-checkmark-circle' })
    const iconType = Platform.select({ android: 'material-community', ios: 'ionicon' }) as TypeIcons

    return <Icon name={ iconName as any } type={ iconType } color={ styles.iconSuccess.color } />
  }

  /**
   * Method that renders the Icon error component
   * @returns {TypeComponent}
   */
  public IconError(): TypeComponent {
    const iconName = Platform.select({ android: 'close-circle', ios: 'ios-close-circle' })
    const iconType = Platform.select({ android: 'material-community', ios: 'ionicon' }) as TypeIcons

    return <Icon name={ iconName as any } type={ iconType } color={ styles.iconError.color } />
  }

  /**
   * Method that renders the MessageInput component
   * @returns {TypeComponent}
   */
  public MessageInput(): TypeComponent {
    const { msgError, msgSuccess } = this._processProps()
    const { error, success } = this.state

    if (success && msgSuccess) {
      return <Text style={ StyleSheet.flatten([styles.message, styles.messageSuccess]) }>{ msgSuccess }</Text>
    }

    if (error) {
      const isDate = this.hasError('date')
      const isEmail = this.hasError('email')
      const isMaxLength = this.hasError('maxLength')
      const isMinLength = this.hasError('minLength')
      const isNumbers = this.hasError('numbers')
      const isRequired = this.hasError('required')
      let message = ''

      if (isRequired) {
        message = this.getError('required') || ''
      } else if (!isRequired && isMinLength) {
        message = this.getError('minLength') || ''
      } else if (!isRequired && isMaxLength) {
        message = this.getError('maxLength') || ''
      } else if ((!isRequired && !isMinLength) && (isEmail || isDate || isNumbers)) {
        if (isDate) {
          message = this.getError('date') || ''
        } else if (isEmail) {
          message = this.getError('email') || ''
        } else if (isNumbers) {
          message = this.getError('numbers') || ''
        }
      }
      return <Text style={ StyleSheet.flatten([styles.message, styles.messageError]) }>{ msgError || message }</Text>
    }
  }

  /**
   * Method that fire when the select changes
   * @private
   * @param {*} value    Current value selected
   * @param {number} index    Current index selected
   * @returns {void}
   */
  private _onChange(value: any, index: number): void {
    const { rules } = this._processProps()
    const { onChange } = this.props

    this.value = value
    this.validate(rules || {})
    this.setState({ value, error: this.hasErrors(), success: !this.hasErrors() })

    if (onChange) {
      return onChange(value, index)
    }
  }

  /**
   * Method to process the props
   * @private
   * @returns {ISelectProps}
   */
  private _processProps(): ISelectProps {
    const { disabled, doneText, fixed, floating, hideDoneBar, hideValidationIcons, inline, items, label, lang, last, regular, rounded, stacked, useNativeAndroidPickerStyle, value } = this.props

    const props: ISelectProps = {
      ...this.props,
      disabled: typeof disabled !== 'undefined' ? disabled : false,
      doneText: typeof doneText !== 'undefined' ? doneText : 'Done',
      fixed: typeof fixed !== 'undefined' ? fixed : false,
      floating: typeof floating !== 'undefined' ? floating : false,
      hideDoneBar: typeof hideDoneBar !== 'undefined' ? hideDoneBar : false,
      hideValidationIcons: typeof hideValidationIcons !== 'undefined' ? hideValidationIcons : true,
      inline: typeof inline !== 'undefined' ? inline : false,
      items: typeof items !== 'undefined' ? items : [],
      lang: typeof lang !== 'undefined' ? lang : 'en',
      last: typeof last !== 'undefined' ? last : false,
      regular: typeof regular !== 'undefined' ? regular : false,
      rounded: typeof rounded !== 'undefined' ? rounded : false,
      stacked: typeof stacked !== 'undefined' ? stacked : false,
      useNativeAndroidPickerStyle: typeof useNativeAndroidPickerStyle !== 'undefined' ? useNativeAndroidPickerStyle : false,
    }

    this.fieldName = label
    this.value = value

    return props
  }
}
