import { StyleSheet } from 'react-native'

const colors = {
  error: '#e8333f',
  success: '#3f8445',
}

export const styles = StyleSheet.create({
  container: {
  },

  item: {
    alignItems: 'center',
    alignSelf: 'center',
    width: '100%',
  },

  itemContainer: {

  },

  iconError: {
    color: colors.error,
  },

  iconSuccess: {
    color: colors.success,
  },

  message: {
    paddingLeft: 8,
    paddingRight: 8,
  },

  messageError: {
    color: colors.error,
  },

  messageSuccess: {
    color: colors.success,
  },
})

export const stylesSelect = StyleSheet.create({
  iconContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    right: 8,
  },

  inputAndroidContainer: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },

  inputAndroid: {
    // borderColor: 'silver',
    // borderRadius: 8,
    // borderWidth: .5,
    color: 'black',
    fontSize: 16,
    // paddingHorizontal: 4,
    paddingLeft: 8,
    // marginLeft: 8,
    paddingRight: 30, // to ensure the text is never behind the icon
    paddingVertical: 11,
    width: '100%',
  },

  inputIOS: {
    // borderColor: 'gray',
    // borderRadius: 4,
    // borderWidth: 1,
    color: 'black',
    fontSize: 16,
    paddingHorizontal: 10,
    paddingRight: 30, // to ensure the text is never behind the icon
    paddingVertical: 12,
  },
})

export default styles
