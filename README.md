# TIC Makers - React Native Select
React native component for select.

Powered by [TIC Makers](https://ticmakers.com)

## Demo

[Select Expo's snack]()

## Install

Install `@ticmakers-react-native/select` package and save into `package.json`:

NPM
```shell
$ npm install @ticmakers-react-native/select --save
```

Yarn
```shell
$ yarn add @ticmakers-react-native/select
```

## How to use?

```javascript
import React from 'react'
import TextAvatar from '@ticmakers-react-native/select'

export default class App extends React.Component {

  render() { }
}
```

## Properties

| Name | Type | Default Value | Definition |
| ---- | ---- | ------------- | ---------- |
| ---- | ---- | ---- | ----

## Todo

- Test on iOS
- Improve and add new features
- Add more styles
- Create tests
- Add new props and components in readme
- Improve README

## Version 1.0.3 ([Changelog])

[Changelog]: https://bitbucket.org/ticmakers/rn-select/src/master/CHANGELOG.md
